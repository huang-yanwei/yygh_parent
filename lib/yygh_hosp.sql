/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : localhost:3306
 Source Schema         : yygh_hosp

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 29/06/2021 20:54:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for hospital_set
-- ----------------------------
DROP TABLE IF EXISTS `hospital_set`;
CREATE TABLE `hospital_set`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `hosname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '医院名称',
  `hoscode` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '医院编号',
  `api_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'api基础路径',
  `sign_key` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '签名秘钥',
  `contacts_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '联系人',
  `contacts_phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '联系人手机',
  `status` tinyint(3) NOT NULL DEFAULT 0 COMMENT '1 使用 0 不能使用',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_deleted` tinyint(3) NOT NULL DEFAULT 0 COMMENT '逻辑删除(1:已删除，0:未删除)',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_hoscode`(`hoscode`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '医院设置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hospital_set
-- ----------------------------
INSERT INTO `hospital_set` VALUES (1, '北京协和医院', '1000_0', 'http://localhost:8201', '6afaf67a0c2a6be0d56271919ae1c105', 'jack', '12345678925', 1, '2021-06-29 13:27:22', '2021-06-29 14:06:57', 0);
INSERT INTO `hospital_set` VALUES (2, '中大五院', '1000_01', 'www.baidu.com', '640c3f15ab153f2527c695dce1d5813a', '张三', '11111111111', 1, '2021-06-22 10:44:54', '2021-06-24 15:13:25', 0);
INSERT INTO `hospital_set` VALUES (3, '金鼎医院', '1000_02', 'www.baidu.com', 'd96fb14a9c1a44e5ff3b4c9217b621b5', '李四', '12378978911', 1, '2021-06-22 10:56:45', '2021-06-24 15:13:00', 0);
INSERT INTO `hospital_set` VALUES (4, '广东省中医院(珠海医院)', '1000_04', 'www.baidu.com', 'dsf53f15ab153f2527c695dce1d5813a', '赵六', '12378123411', 1, '2021-06-23 22:40:19', '2021-06-24 15:13:04', 0);
INSERT INTO `hospital_set` VALUES (5, '协和医院', '1000_5', 'www.baidu.com', 'f88dc20d712145e729cf3befc302387b', '熊大', '15462789453', 1, '2021-06-24 16:29:06', '2021-06-24 16:29:06', 0);
INSERT INTO `hospital_set` VALUES (7, 'aaa', 'aaa', 'aaa', '99772c41df6bbdbfcf984b268b597bdd', 'aa', 'aaa', 1, '2021-06-24 18:58:18', '2021-06-24 18:58:25', 1);
INSERT INTO `hospital_set` VALUES (8, '珠海市人民医院a', '1000_03a', 'www.baidu.coma', '0b6c35fb7047592fb645521f419def9d', '王五a', '12312378911', 1, '2021-06-23 22:37:53', '2021-06-24 15:13:27', 0);
INSERT INTO `hospital_set` VALUES (9, '珠海市人民医院b', '1000_03b', 'www.baidu.comb', 'd96fb14a9c1a55e5ff3b4c9276b621b5', '王五b', '12312378911', 1, '2021-06-23 22:37:53', '2021-06-29 14:07:01', 0);

SET FOREIGN_KEY_CHECKS = 1;
