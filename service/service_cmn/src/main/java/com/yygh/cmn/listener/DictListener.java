package com.yygh.cmn.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.yygh.cmn.mapper.DictMapper;
import com.yygh.model.cmn.Dict;
import com.yygh.vo.cmn.DictEeVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.BeanUtils;

@AllArgsConstructor
@Data
public class DictListener extends AnalysisEventListener<DictEeVo> {
    private DictMapper dictMapper;

    //一行一行读取,从第二行开始
    @Override
    public void invoke(DictEeVo dictEeVo, AnalysisContext analysisContext) {
        //调用方法添加数据库
        Dict dict = new Dict();
        BeanUtils.copyProperties(dictEeVo, dict);
        dictMapper.insert(dict);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
