package com.yygh.hosp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yygh.model.hosp.HospitalSet;

public interface HospitalSetMapper extends BaseMapper<HospitalSet> {
}
