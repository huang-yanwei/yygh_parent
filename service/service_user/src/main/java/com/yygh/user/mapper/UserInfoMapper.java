package com.yygh.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yygh.model.user.UserInfo;

public interface UserInfoMapper extends BaseMapper<UserInfo> {
}
