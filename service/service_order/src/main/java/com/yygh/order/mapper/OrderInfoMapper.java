package com.yygh.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yygh.model.order.OrderInfo;

public interface OrderInfoMapper extends BaseMapper<OrderInfo> {
}
